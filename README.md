# Stateful Discord Bot Stack

note, some of the bots used here are in a private git repository.
make sure you are using authenticated credentials to pull them into this repo

### Set up

Clone each bot into this project directory, for example:

```sh
git clone https://git.ssh.surf/MrTuxedo/gwei-alert-bot/
```

Populate env vars for each bot in `.env`: see [.env.sample](./.env.sample) for expected values.

### Spin up bot stack

```sh
docker compose up -d
```

### Updates

Sometimes you may want to rebuild a bot to incorporate upstream changes without interupting the other services in the stack.  To do this:

```sh
cd gwei-alert-bot
git pull
cd ..
docker compose up gwei-alert-bot --build --force-recreate -d
```